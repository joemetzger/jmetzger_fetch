# README #

### Pyramid Word Checker ###

* Web service to check whether words are pyramid words.
* A pyramid word is a word whose character frequencies form a continuous range from 1 without duplicates or gaps.

### Run ###

* Option 1 : .\mvnw spring-boot:run
* Option 2 : .\mvnw clean package  
             .\java -jar target\pyramid-word-0.0.1-SNAPSHOT.jar

### Usage ###
* HTTP GET http://localhost:8080/check?word={word}
* Returns json response with result:true if word is a pyramid word, false otherwise
* Optional query param case=false to disable case sensitivity ('B' === 'b')
