package com.jmetzger_fetch.pyramidword;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class PyramidWordCheckerTests {

    /**
     * Validate pyramid word checker against null and empty input.
     */
    @Test
    public void testPyramidWordCheckerEmptyInput() {
        PyramidWordChecker checker = new PyramidWordChecker(null, true);
        assertTrue(checker.getResult(), "Result should be true for null word.");

        checker = new PyramidWordChecker("", true);
        assertTrue(checker.getResult(), "Result should be true for empty word.");
    }

    /**
     * Validate pyramid word checker against some example inputs.
     */
    @Test
    public void testPyramidWordCheckerExampleInputs() {
        PyramidWordChecker checker;
        Map<String, Boolean> examples = new HashMap<String, Boolean>() {{
            put("banana", true);
            put("BANANA", true);
            put("bandana", false);
            put("bananaa", false);
            put("bananb", false);
            put("aaaabbbccd", true);
            put("aba", true);
        }};

        for (Map.Entry<String, Boolean> entry : examples.entrySet()) {
            String testWord = entry.getKey();

            checker = new PyramidWordChecker(testWord, true);
            boolean testResult = checker.getResult();

            assertEquals(
                testResult,
                entry.getValue(),
                String.format("Unexpected result for %s: %b", testWord, testResult));
        }
    }

    /**
     * Validate pyramid word checker with case-sensitive/insensitive flag.
     */
    @Test
    public void testPyramidWordCheckerCaseSensitivity() {
        PyramidWordChecker checker = new PyramidWordChecker("Banana", true);
        assertTrue(checker.getResult(), "Banana should be a pyramid word.");

        checker = new PyramidWordChecker("BanaNa", true);
        assertFalse(checker.getResult(), "BanaNa should not be a pyramid word with case sensitivity.");

        checker = new PyramidWordChecker("BanaNa", false);
        assertTrue(checker.getResult(), "BanaNa should be a pyramid word with case insensitivity.");
    }
}
