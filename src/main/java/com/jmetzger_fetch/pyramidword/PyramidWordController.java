package com.jmetzger_fetch.pyramidword;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PyramidWordController {

    @GetMapping("/check")
    public PyramidWordChecker checkWord(
            @RequestParam(value = "word", defaultValue = "") String word,
            @RequestParam(value = "case", required = false, defaultValue = "true") boolean caseSensitive)
    {
        return new PyramidWordChecker(word, caseSensitive);
    }
}