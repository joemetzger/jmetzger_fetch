package com.jmetzger_fetch.pyramidword;

import java.util.HashMap;
import java.util.HashSet;

/**
 * PyramidWordChecker - Checks whether a given word is a 'pyramid' word.
 *   A word is a ‘pyramid’ word if you can arrange the letters in increasing
 *   frequency, starting with 1 and continuing without gaps and without duplicates.
 *
 *   IE - "banana" is a pyramid word (1b, 2n, 3a)
 *   IE - "bandana" is not a pyramid word (1b, *1d*, 2n, 3a)
 */
public class PyramidWordChecker {
    private final String word;
    private final boolean result;

    /**
     * Constructor
     * @param word : String, word to check as a pyramid word
     * @param caseSensitive : Boolean, if true, we consider character case
     *                      when counting frequencies ('B' != 'b')
     */
    public PyramidWordChecker(String word, boolean caseSensitive) {
        this.word = word;
        this.result = this.isPyramidWord(word, caseSensitive);
    }

    /**
     * Getter for word
     * @return String Word being analyzed
     */
    public String getWord() {
        return word;
    }

    /**
     * Getter for result
     * @return boolean True if word is a pyramid word, false otherwise.
     */
    public boolean getResult() {
        return result;
    }

    /**
     * IsPyramidWord - Check the given word for the pyramid word property.
     *
     *   Approach:
     *      0) If we desire case-insensitivity, convert the word to lowercase.
     *      1) Count character frequencies in the word.
     *      2) Create a set of the frequencies. If any duplicate frequencies are present
     *           we cannot have a pyramid word so return false.
     *      3) Validate that the (now unique) frequencies form a continuous range from
     *           1 - #_of_frequencies. If so, return true, otherwise word is not a pyramid word.
     *
     *   Runtime:
     *      0) O(n)
     *      1) O(n), hashmap insert/get are O(1) for n chars
     *      2) O(n), set insert/contains are O(1) for n chars
     *      3) O(n), set contains is O(1) for maximum n characters (although we'd have failed already in that case
     *      Total: O(n)
     *
     *      Space: O(n) for the additional hashmap & set
     *
     * @param word : String, The word to check
     * @param caseSensitive : boolean, Whether to consider case when counting characters
     * @return boolean : True if word is a pyramid word, false otherwise.
     */
    private boolean isPyramidWord(String word, boolean caseSensitive) {
        if (word == null || word.length() == 0) {
            return true;
        }

        if (!caseSensitive) {
            word = word.toLowerCase();
        }

        // Build frequency map of word characters and a subsequent set of those frequencies
        // If any duplicate counts exist, the word cannot be a pyramid word
        HashMap<Character, Integer> charFrequencies = new HashMap<Character, Integer>();
        for (char c : word.toCharArray()) {
            charFrequencies.put(c, charFrequencies.getOrDefault(c, 0) + 1);
        }

        HashSet<Integer> counts = new HashSet<Integer>();
        for (int freqCount : charFrequencies.values()) {
            if (counts.contains(freqCount)) {
                return false;
            }

            counts.add(freqCount);
        }

        // Character frequencies are unique (enforced above) and should be in the range
        // [1, n] where n is the # of distinct characters in the word
        for (int currentCount = 1; currentCount <= counts.size(); currentCount++) {
            if (!counts.contains(currentCount)) {
                return false;
            }
        }

        return true;
    }
}
